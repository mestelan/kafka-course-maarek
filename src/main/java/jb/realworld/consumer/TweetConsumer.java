package jb.realworld.consumer;

import jb.realworld.producer.ProducerFactory;
import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 *
 */
public final class TweetConsumer {

    public static final String GROUP_ID = "tweetor";
    public static final List<String> TOPICS = Arrays.asList(ProducerFactory.TOPIC);
    private final Logger logger = LoggerFactory.getLogger(TweetConsumer.class.getName());
    private final
    RestHighLevelClient client = new RestHighLevelClient(
            RestClient.builder(
                    new HttpHost("localhost", 9200, "http"),
                    new HttpHost("localhost", 9201, "http")));

    private final Properties properties = getProperties();
    ActionListener<IndexResponse> listener=getListener();

    public void consume() {


        KafkaConsumer<String, Object> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(TOPICS);

        while (true) {
            ConsumerRecords<String, Object> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, Object> record : records) {
                logger.info("Received new metadata: \n" +
                        "Topic: " + record.topic() +
                        "\n" +
                        "Key: " + record.key() +
                        "\n" +
                        "Value: " + record.value() +
                        "\n" +
                        "Partition: " + record.partition() +
                        "\n" +
                        "Offset: " + record.offset() +
                        "\n" +
                        "Timestamp: " + record.timestamp() +
                        "\n");
                insertIntoElastic(record);
            }
        }
    }
    /**
     * https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-document-index.html
     */
    private void insertIntoElastic(ConsumerRecord<String, Object> record) {
        IndexRequest request = new IndexRequest(
                "posts",
                "doc",
                "1");
        String jsonString = "{" +
                "\"blob\":" + record.toString() +
                "}";
        request.source(jsonString, XContentType.JSON);


        client.indexAsync(request, RequestOptions.DEFAULT, listener);
    }

    private Properties getProperties() {
        Properties properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, ProducerFactory.BOOTSTRAP_SERVERS);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        return properties;
    }

    private ActionListener<IndexResponse> getListener() {
        return new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
logger.info(indexResponse.toString());
            }

            @Override
            public void onFailure(Exception e) {
                logger.error("Error while inserting to EL.",e);
            }
        };
    }

}
