package jb.realworld.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class TweetProducer {
    final Logger logger = LoggerFactory.getLogger(TweetProducer.class);
    final KafkaProducer<String, Object> producer = ProducerFactory.create();

    public void produceRecord(Object tweetRecord) {
        ProducerRecord<String, Object> record = new ProducerRecord<>(ProducerFactory.TOPIC, tweetRecord);
        // Executes every time a record is successfully sent or an exception is thrown.
        producer.send(record, (recordMetadata, e) -> {
            if (e == null)
                logger.info("sent record: " + tweetRecord);
            else
                logger.error("Error while producing.", e);
        });
    }

    public void close() {
        // Flush and close
        producer.close();
    }

}
