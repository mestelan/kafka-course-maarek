package jb.simple;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * You can start two of thee, and witness the effect of re-balancing on partitions.
 */
public final class ConsumerDemoWithThreads_TODO {
    public static final String BOOTSTRAP_SERVERS = "127.0.0.1:9092";
    public static final String GROUP_ID = "my_fourth_application";
    public static final List<String> TOPICS = Arrays.asList("first_topic", "second_topic");

    public static void main(String[] args) {
        ConsumerDemoWithThreads_TODO x = new ConsumerDemoWithThreads_TODO();
        x.consume();
    }
    private final Logger logger = LoggerFactory.getLogger(ConsumerDemoWithThreads_TODO.class.getName());

    private void consume() {
        Properties properties= new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
        consumer.subscribe(TOPICS);

        while (true){
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record : records) {
                logger.info("Received new metadata: \n"+
                        "Topic: "+ record.topic()+
                        "\n"+
                        "Key: "+ record.key()+
                        "\n"+
                        "Value: "+ record.value()+
                        "\n"+
                        "Partition: "+  record.partition()+
                        "\n"+
                        "Offset: "+ record.offset()+
                        "\n"+
                        "Timestamp: "+ record.timestamp()+
                        "\n");
            }
        }
    }
}
