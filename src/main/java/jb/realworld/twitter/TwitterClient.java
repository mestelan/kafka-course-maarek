package jb.realworld.twitter;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.event.Event;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;

/**
 * See https://github.com/twitter/hbc
 */
public final class TwitterClient {
    final BlockingQueue<String> queue;
    final Client client;

    public TwitterClient() {
        this(100_000);
    }

    public TwitterClient(int queueLength) {
        this.queue = new LinkedBlockingQueue<String>(queueLength);
        this.client = buildClient(queue);
    }

    public void connect() {
        // Attempts to establish a connection.
        client.connect();
    }

    public void close() {
        client.stop();
    }

    /**
     * Until interrupted:
     * - reads tweets from the queue,
     * - 'consumes' tweets, sending them to Kafka producer.
     */
    public void consumeStream(Consumer<Object> aConsumer) {
        while (!client.isDone()) {
            String msg = null;
            try {
                msg = queue.take();
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }
            aConsumer.accept(msg);
        }
    }

    private Client buildClient(BlockingQueue<String> msgQueue) {
        BlockingQueue<Event> eventQueue = new LinkedBlockingQueue<Event>(1_000);
        /* Declares the host you want to connect to,
         * the endpoint, and authentication (basic auth or oauth) */
        Hosts hosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint endpoint = new StatusesFilterEndpoint();
        // Optional: set up some followings and track terms
//        List<Long> followings = Lists.newArrayList(1234L, 566788L);
//        endpoint.followings(followings);
        List<String> terms = Lists.newArrayList("bitcoin", "ethereum");
        endpoint.trackTerms(terms);

        // These secrets should be read from a config file
        String consumerKey = "MFraCWMuO5wyfHsrXqK0g";
        String consumerSecret = "bHxNm9bFwRUrXgY9KLjXCW7y19Z0dRsE9E7pG1SdEHY";
        String token = "455793592-naHLrqMMgwzQ345ky81L2ueGoSecQTkH9im60ufg";
        String secret = "RfA0IfwTpG84rWfy6WpQkclofbVkK05KqEzZOLtIOCrf2";

        Authentication auth = new OAuth1(consumerKey, consumerSecret, token, secret);

        ClientBuilder builder = new ClientBuilder()
                .name("Hosebird-Client-01")                              // optional: mainly for the logs
                .hosts(hosts)
                .authentication(auth)
                .endpoint(endpoint)
                .processor(new StringDelimitedProcessor(msgQueue))
                .eventMessageQueue(eventQueue);                          // optional: use this if you want to process client events

        return builder.build();
    }

}
