package jb.simple;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class ProducerDemoWithKeys {

    public static final String BOOTSTRAP_SERVERS = "127.0.0.1:9092";
    Logger logger = LoggerFactory.getLogger(ProducerDemoWithKeys.class);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ProducerDemoWithKeys x = new ProducerDemoWithKeys();
        x.produce();
    }

    private void produce() throws ExecutionException, InterruptedException {
        //
        Properties properties= new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i = 0; i < 10; i++) {

            String topic = "first_topic";
            String value = "message #" + i;
            String key = "id_" + i;
            ProducerRecord<String, String> record= new ProducerRecord<String, String>(topic, key, value);
            logger.info("Key: " + key);
            // The same key always goes to the same partition
            // id_0 is always going to partition 1
            // id_1 is always going to partition 0
            // id_2 is always going to partition 2
            // ...
            producer.send(record, new Callback() {
                // Executes every time a record is successfully sent or an exception is thrown.
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    if (e==null)
                        logger.info("Received new metadata: \n"+
                                "Topic: "+ recordMetadata.topic()+
                                "\n"+
                                "Partition: "+  recordMetadata.partition()+
                                "\n"+
                                "Offset: "+ recordMetadata.offset()+
                                "\n"+
                                "Timestamp: "+ recordMetadata.timestamp()+
                                "\n");
                    else
                        logger.error("Error while producing.", e);
                }
            }).get(); // Blocks the .send() to make it synchronous - don't do this in production!
        }
        // flush and close
        producer.close();
    }
}
