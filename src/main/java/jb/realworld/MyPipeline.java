package jb.realworld;

import jb.realworld.consumer.TweetConsumer;
import jb.realworld.producer.TweetProducer;
import jb.realworld.twitter.TwitterClient;

public final class MyPipeline {
    public static void main(String[] args) {
        MyPipeline x = new MyPipeline();
        x.runPipeline();
    }

    private void runPipeline() {
//        TweetConsumer tweetConsumer = new TweetConsumer();
//        tweetConsumer.consume();

        TweetProducer tweetProducer = new TweetProducer();
        TwitterClient twitterClient = new TwitterClient();
        twitterClient.connect();
        twitterClient.consumeStream(tweetProducer::produceRecord);

        tweetProducer.close();
        twitterClient.close();
    }
}