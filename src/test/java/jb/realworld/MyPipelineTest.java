package jb.realworld;



import jb.realworld.consumer.TweetConsumer;
import jb.realworld.producer.TweetProducer;
import jb.realworld.twitter.TwitterClient;
import org.junit.jupiter.api.Test;

public final class MyPipelineTest {
    @Test
    void runPipeline() {
        TweetConsumer tweetConsumer = new TweetConsumer();
        tweetConsumer.consume();

        TweetProducer tweetProducer = new TweetProducer();
        TwitterClient twitterClient = new TwitterClient();
        twitterClient.connect();
        twitterClient.consumeStream(tweetProducer::produceRecord);

        tweetProducer.close();
        twitterClient.close();
    }
}
