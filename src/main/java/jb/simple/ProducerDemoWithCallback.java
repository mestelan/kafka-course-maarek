package jb.simple;

import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ProducerDemoWithCallback {

    public static final String BOOTSTRAP_SERVERS = "127.0.0.1:9092";
    Logger logger = LoggerFactory.getLogger(ProducerDemoWithCallback.class);

    public static void main(String[] args) {
        ProducerDemoWithCallback x = new ProducerDemoWithCallback();
        x.produce();
    }

    private void produce() {
        //
        KafkaProducer<String, String> producer = createProducer();

        ProducerRecord<String, String> record= new ProducerRecord<>("first_topic", "hello");
        producer.send(record, new Callback() {
            // Executes every time a record is successfully sent or an exception is thrown.
            public void onCompletion(RecordMetadata recordMetadata, Exception e) {
               if (e==null)
           logger.info("zorg topic: " + recordMetadata.topic());
               else
                   logger.error("Error while producing.", e);
            }
        });
        // flush and close
        producer.close();
    }

    private KafkaProducer<String, String> createProducer() {
        Properties properties= new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        return new KafkaProducer<>(properties);
    }
}
